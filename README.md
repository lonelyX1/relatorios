# Apresentação

+ Nome: Pedro Henrique Kauan Alonso Mendes
+ Idade: 19
+ Graduação: Engenharia de Computação(Cursando ⚠️)
+ Contato: 67984747969
+ Rede social: @pedromendeswr(Instagram)
+ E-mail: pedro.mendes.pb@compasso.com.br

## Estágio:

Vou organizar da seguinte maneira, colocar as coisa importantes aqui nesse README.

+ Sprint 1: Processos Ágeis & AWS ✅
+ Sprint 2: Análise e Testes de APIs REST ✅
+ Sprint 3: Análise, Planejamento e Execução de Testes ✅
+ Sprint 4: Testes Exploratórios e JavaScript ✅
+ Sprint 5: Fundamentos de Testes de Performance ✅ 
+ Sprint 6: Primeiros passos com K6
+ Sprint 7: Avançando com K6
+ Sprint 8: Docker, AWS, Challenge Final
+ Sprint 9 e 10: Estudos AWS

## Branch:

As Branch estão separados, cada sprint do estágio foi criado uma, e dentro delas 
está os conteúdos aprendidos na sprint. 

+ main
+ pb_sprint2
+ pb_sprint3
+ pb_sprint4
+ pb_sprint5
+ pb_sprint6

Dentro dessas Branch está os conteúdos aprendidos na sprint que foi feita.
